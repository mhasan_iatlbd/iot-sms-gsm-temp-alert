#include <SoftwareSerial.h>
#include <LiquidCrystal.h> 
#include <EEPROM.h>
SoftwareSerial mySerial(7, 8); // SoftwareSerial RX, TX
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

int alarmPin = 13;
int gsmresetPin=10;
int tempPin = 5;
//bool smokep=true;
bool gsmp=true;
int reading;
float tempC;
int threshold=35;

//const int pinsns=3;
bool sms;
char smsIn;
char smsbuffer[160];
int i=0,cmd=0,l=0,nc=0,netstat=0;
long r=0,q=0;
String smsins;
String sm="+CMTI:";
String pb="+CPBR:";
String sr="+CMGR:";
String gs="+CMGS:";
String rg="+CGREG";
String cq="+CSQ:";
String tx="Temp";
String ts="Tresh:";
String phonenumber[4]={"\"01939900090\"","","",""};
int index=0;
String phonenumberps="";
bool gsmready=false;
 int phoneindex=0;
 bool alarm=false,alarmsms1=false,alarmsms2=false,alarmsms3=false,alarmsms4=false;
 int smsindex=0;
 int address = 0;
void setup()
{
  
  analogReference(INTERNAL);  
  pinMode(alarmPin, OUTPUT);
  pinMode(gsmresetPin, OUTPUT);
  digitalWrite(gsmresetPin, HIGH);
  digitalWrite(alarmPin, HIGH);
 //pinMode(pinsns,INPUT);
 //EEPROM.put(address, threshold);
 EEPROM.get(address, threshold);
 lcd.begin(20, 4);
 lcd.setCursor(0, 0);
   lcd.print("       STC&S  ");
  // lcd.print("       IAT Ltd  ");
  lcd.setCursor(14, 0);
  lcd.print("T");
  lcd.setCursor(15, 0);
  lcd.print(threshold);
   lcd.setCursor(0, 1);
   lcd.print("Capt Md Raqibul,sigs");
  // lcd.print("Inspired By Creative");
   //lcd.setCursor(0, 2);
   //lcd.print("Hoque, sigs");
   //delay(5000);
   lcd.setCursor(0, 2);
   lcd.print("DIGITAL TEMP METER");
   //lcd.setCursor(0, 2);
   //lcd.print("     METER");
   lcd.setCursor(0, 3);
   lcd.print("TEMP:     C. ALARM:0");
   sms=false;
   readSensor();
   delay(5000);
   mySerial.begin(9600);               // the GPRS baud rate   
   Serial.begin(9600);                 // the GPRS baud rate 
   mySerial.println("AT+CMGF=1\r");
   digitalWrite(alarmPin, LOW);
}
 
void loop()
{
 // readSensor();

  
  if(i==0)
    smsins="";
 if (mySerial.available()) {
    int inByte = mySerial.read();
    Serial.write(inByte); 
    if(inByte==13){
     // Serial.write("r");
      
      smsbuffer[i++]=inByte;
      
      
      //String ins(smsbuffer);
      //int r=sm.compareTo(smsins);
      smsins.trim();
      String ss=smsins.substring(0,6);
      //Serial.println(smsins);
      ss.trim();
      //sm.trim();
      Serial.println(ss);
     // Serial.println(sm);
      if(sm.equals(ss)){
        // Serial.println("SMS Received");
        lcd.setCursor(0,1);
        lcd.print("SMS Received");
        String sind=smsins.substring(12);
        sind.trim();
        index=sind.toInt();
        // Serial.print("Index:");Serial.println(sind);
        mySerial.println("AT+CMGR="+sind+"\r");
        // delay(1000);
       cmd=1;
      }else if(pb.equals(ss)){  //AT+CPBR=1
        Serial.print("Phone book read:");Serial.println(phoneindex);
        String entry=getValue(smsins,',',1);
        entry.replace(",","");
        entry.trim();
        phonenumber[phoneindex]=entry;
        phoneindex++;
      //  Serial.println("Number:"+phonenumber[0]);
      }else if(ts.equalsIgnoreCase(ss)){
         if(checkNumber(phonenumberps)){
          String thres=smsins.substring(6);
          Serial.println("Threshold:"+thres);
          threshold=thres.toInt();
          EEPROM.update(address, threshold);
          lcd.setCursor(14, 0);
          lcd.print("T   ");
          lcd.setCursor(15, 0);
          lcd.print(threshold);
          lcd.setCursor(0,1);
          lcd.print("Threshold:      ");
          lcd.setCursor(11,1);
          lcd.print(threshold);
         }
          mySerial.print("AT+CMGD=");mySerial.print(index);mySerial.println(",4");
          delay(1000);
         
      }else if(tx.equalsIgnoreCase(ss)){
        
       // Serial.println("SMS Command:"+tx+"  Phone:"+phonenumberps);
        //mySerial.print("AT+CMGF=1\r");
        readSensor();
        delay(1000);
        if(checkNumber(phonenumberps)){
          mySerial.print("AT+CMGS="+phonenumberps+"\r");//phonenumber[0]);
          delay(500);
          ShowSerialData();       
          mySerial.print("Temperature ");mySerial.print(tempC);mySerial.print(" Degree Celcius\r");// message
          delay(500);
          ShowSerialData();
          mySerial.write(0x1A); //send a Ctrl+Z(end of the message)
          //delay(100);
          ShowSerialData();
        // mySerial.print("AT+CMGD=");mySerial.println(index);
         delay(1000);
          cmd=2;
        }else{
          mySerial.print("AT+CMGD=");mySerial.print(index);mySerial.println(",4");
          //delay(1000);
          ShowSerialData();
        }
      }else if(sr.equals(ss)){
       // Serial.println("Get Mobile number from SMS");
        String entry=getValue(smsins,',',1);
        entry.replace(",","");
        phonenumberps=entry;
        phonenumberps.trim();
         lcd.setCursor(0,1);
      lcd.print(phonenumberps);
       // Serial.println("Number:"+phonenumberps);
        
      }else if(gs.equals(ss)){
        //mySerial.print("AT+CMGD=");mySerial.println(index);
        //  delay(1000);
        
        if(alarm==true)
          smsindex=smsindex+1;
        else
          cmd++;
        
      }else if(rg.equals(ss)){
        String stat=smsins.substring(10);
        stat.trim();
        netstat=stat.toInt();
        Serial.print("Network Status:");Serial.println(stat);
        lcd.setCursor(0,0);
        lcd.print(stat);
      }else if(cq.equals(ss)){
        
        String net=smsins.substring(6);
        //Serial.print("Network quality:");Serial.println(net);
        String nets=getValue(net,',',0);
        nets.replace(",","");
        Serial.print("Network quality:");Serial.println(nets);
        lcd.setCursor(18,0);
        lcd.print(nets);
      }else if(smsins.equals("OK")){
      
        Serial.print("Command Success cmd=");Serial.println(cmd);
        if(gsmready == false){
          lcd.setCursor(0,1);
          lcd.print("GSM Module Ready....");
          delay(5000);
          mySerial.println("AT+CPBR=1,4\r");
          gsmready=true;
        }
        if(cmd==4){
          //mySerial.println("AT+CMGR=?");
        //  Serial.println("SMS Send:");
          mySerial.print("AT+CMGD=");mySerial.print(index);mySerial.println(",4");
          cmd++;
        }else if(cmd==5){
        //  Serial.println("SMS Deleted:");
          lcd.setCursor(0,1);
          lcd.print("SMS Response Send");
          cmd++;
        }
      }else{
       /* if(cmd==2){
          mySerial.println("Temperature  Degree Celcius");// message
          delay(1000);
          cmd++;
        }else 
        */if(cmd==2){
         // mySerial.write(0x1A); //send a Ctrl+Z(end of the message)
         // delay(1000);
          //mySerial.print("AT+CMGD=");mySerial.println(index);
          //delay(1000);
          cmd++;
        }else if(cmd==3){
        //  mySerial.print("AT+CMGD=");mySerial.println(index);
         // delay(1000);
         // cmd=0;
        }
      }
      i=0;
    }else{
      smsbuffer[i++]=inByte;
      char c=inByte;
      smsins+=String(c);
    }
    //if(inByte==10)
     // Serial.write("n");
    
  }else{
    //Serial.write("n");
    l++;
    r++;
    q++;
    if(l>9000){
      readSensor();
      l=0;
    }
    if(r>200000){
      getRegLevel();
      if(netstat!=1){
        nc++;
        if(nc>10){
          //SIM 900 Reset
           lcd.setCursor(3,0);
           lcd.print("R");
           digitalWrite(gsmresetPin, LOW);
           Serial.println("Network Reset#################");
           delay(500);
           digitalWrite(gsmresetPin, HIGH);
          nc=0;
        }
      }
      if(nc>0 && netstat==1)
        nc=0;
       
        
      
      r=0;
    }
    if(q>300000){
      getSignalLevel();
      q=0;
      if(nc>0 && netstat==1)
        nc=0;
      if(phoneindex==0)
        mySerial.println("AT+CPBR=1,4\r");
    }
  }
  if (Serial.available()) {
    int inByte = Serial.read();
    smsIn=inByte;
    if(inByte==90)
      mySerial.write(0x1A);
    mySerial.write(inByte); 
  }
  //else{
    //Serial.write("s");
  //}
 // sms=digitalRead(pinsns);
 // if(smsIn=='V'){
 //   mySerial.write(0x1A);
  //  Serial.write("<ctrl+Z>");
  //}
}
void readSensor(){
 // smokep = digitalRead(smokePin);
  reading = analogRead(tempPin);
  tempC = reading / 9.31;

  lcd.setCursor(5, 3);
   lcd.print(tempC);
   //lcd.setCursor(19, 3);
   //lcd.print(smokep);

   if(gsmready==true){
    if(tempC>threshold && alarm==false && alarmsms1==false){
      smsindex=0;
     
         sendSMS();
       alarmsms1=true;
       alarm=true;
       lcd.setCursor(19, 3);
       lcd.print("1");
       digitalWrite(alarmPin, HIGH);
    }else if(tempC>threshold && alarm==true && smsindex==1 && smsindex<4 && alarmsms2==false){
      sendSMS();
      
      alarmsms2=true;
    }else if(tempC>threshold && alarm==true && smsindex==2 && smsindex<4 && alarmsms3==false){
      sendSMS();
      alarmsms3=true;
    }else if(tempC>threshold && alarm==true && smsindex==3 && smsindex<4 && alarmsms4==false){
      sendSMS();
      alarmsms4=true;
    }else if(alarm==true){ 
      int tts=threshold-1;      
      if(tempC<tts){
        alarm=false;
        alarmsms1=false;
        alarmsms2=false;
        alarmsms3=false;
        alarmsms4=false;
        lcd.setCursor(19, 3);
        lcd.print("0");
        digitalWrite(alarmPin, LOW);
      }
    }
   }
}
void getSignalLevel(){
  mySerial.print("AT+CSQ\r");
}
void getRegLevel(){
  netstat=6;
  mySerial.print("AT+CGREG?\r");
}
void sendSMS(){
  int ln=phonenumber[smsindex].length();
         if(ln>=13){
          mySerial.print("AT+CMGS="+phonenumber[smsindex]+"\r");//phonenumber[0]);
          Serial.println("AT+CMGS="+phonenumber[smsindex]+"\r");
          delay(500);
          ShowSerialData();
          mySerial.print("Temperature threshold exceed: Current temperature ");mySerial.print(tempC);mySerial.print(" Degree Celcius\r");// message
          delay(500);
          ShowSerialData();
          mySerial.write(0x1A); //send a Ctrl+Z(end of the message)
          delay(1000);
          ShowSerialData();
         }
}
bool checkNumber(String phoneno){
  
  String ph=phoneno;
  ph.replace("\"","");
  int pl=ph.length();
  if(pl>11)
    ph=ph.substring(pl-11);

  Serial.println("Number1:"+ph);
  int k=0;
  for(k=0;k<4;k++){
    String pph=phonenumber[k];
    pph.replace("\"","");
    int sl=pph.length();
    
    if(sl>11)
      pph=pph.substring(sl-11);
    Serial.println("Number2:"+pph);
    if(sl>=11 && pph.equals(ph))
      return true;    
  }
  return false;
}
String getValue(String data, char separator, int index)
{
    int maxIndex = data.length() - 1;
    int j = 0;
    String chunkVal = "";

    for (int i = 0; i <= maxIndex && j <= index; i++)
    {
        chunkVal.concat(data[i]);

        if (data[i] == separator)
        {
            j++;

            if (j > index)
            {
                chunkVal.trim();
                return chunkVal;
            }

            chunkVal = "";
        }
        else if ((i == maxIndex) && (j < index)) {
            chunkVal = "";
            return chunkVal;
        }
    }   
}

void ShowSerialData()
{
  while(mySerial.available()!=0)
    Serial.write(mySerial.read());
}
